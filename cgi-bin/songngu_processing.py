#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Import modules for CGI handling
import cgi, cgitb
import os, sys
import glob
import shutil
import re
import codecs

cgitb.enable()

cgi_bin_dir = os.getcwd()
aligner_scripts_dir = cgi_bin_dir.replace('/cgi-bin', '/songngu/aligner/scripts')
os.environ['LD_LIBRARY_PATH'] = aligner_scripts_dir  # for web. Additional libraries for hunalign are put there.
pdftotext_dir = aligner_scripts_dir + '/pdftotext'
os.environ['PATH'] += ':' + pdftotext_dir
if not os.path.isdir(cgi_bin_dir + '/gl'):  # the directory containing library 32-bit ld-linux.so.2 does not exist
    # Create a symbolic link for gl that has 32-bit ld-linux.so.2 needed for specific 32-bit pdftotext binary, which relies on relative ./gl directory
    os.symlink(pdftotext_dir + '/gl', cgi_bin_dir + '/gl')

server_name = os.environ["SERVER_NAME"]
#BASE_URL = 'http://localhost:8000'.rstrip('/')
BASE_URL = 'http://'+server_name+'/songngu'.rstrip('/')  # for web
TOP_DIR = '../songngu'.rstrip('/')
ALIGNER_DIR = 'aligner'
MODULE_DIR = 'modules'.rstrip('/')
DATA_DIR = 'data'
UPLOAD_DIR = 'uploads'.rstrip('/')
TEMP_RESULT_DIR = 'results'.rstrip('/')
OUTPUT_DIR = 'downloads'.rstrip('/')
ABS_TOP_DIR = cgi_bin_dir.replace('/cgi-bin', '/songngu').rstrip('/')
OUTPUT_FILE = 'results.zip'
USER_NOTE_FILE = 'note.txt'
LANGUAGE_DICT = {'de':'deutsch', 'en':'english', 'fr':'francais', 'vi':'tieng-viet'}  # slugs of tags in Wordpress blog

# Note: running on localhost:8000, the TOP_DIR is '.', because the current directory
# is with the HTML file that calls to this Python script (upper dir)
# While running on web server of Hawkhost, TOP_DIR is '..'

from sys import path
path.append(TOP_DIR+'/'+MODULE_DIR)

import Textparser
import Pdfsplitter
import Auto_Align_Compare
import Csv_Html
import Compare_Text
import Text_Tools
import Email_Tools
import Url_Text

def create_user_dir(base_dir):
    """Create directory for the request, with the structure
    base_dir/YYYY/MM/DD/hour-minute-second, and output the relative path
    """
    from datetime import datetime
    # Create directory

    def generate_parent_dir():
        today = datetime.utcnow()

        relative_path_dir_year = str(today.year)
        relative_path_dir_month = os.path.join(relative_path_dir_year, str(today.month))
        relative_path_dir_day = os.path.join(relative_path_dir_month, str(today.day))
        relative_path_dir_now = os.path.join(relative_path_dir_day, str(today.hour)+'-'+str(today.minute)+'-'+str(today.second))

        path_dir_year = os.path.join(base_dir, relative_path_dir_year)
        path_dir_month = os.path.join(base_dir, relative_path_dir_month)
        path_dir_day = os.path.join(base_dir, relative_path_dir_day)

        if not os.path.exists(path_dir_year):
            os.makedirs(path_dir_year)

        if not os.path.exists(path_dir_month):
            os.makedirs(path_dir_month)

        if not os.path.exists(path_dir_day):
            os.makedirs(path_dir_day)

        return relative_path_dir_now


    relative_path_dir_time = generate_parent_dir()

    while os.path.exists(os.path.join(base_data_dir, relative_path_dir_time)):
        relative_path_dir_time = generate_parent_dir()

    os.makedirs(os.path.join(base_data_dir, relative_path_dir_time))

    return relative_path_dir_time


base_data_dir = os.path.join(TOP_DIR, DATA_DIR)
relative_user_dir = create_user_dir(base_data_dir)

os.makedirs(os.path.join(base_data_dir, relative_user_dir, UPLOAD_DIR))
os.makedirs(os.path.join(base_data_dir, relative_user_dir, TEMP_RESULT_DIR))
os.makedirs(os.path.join(base_data_dir, relative_user_dir, OUTPUT_DIR))

# Create instance of FieldStorage, it can only be initiated once per request
form = cgi.FieldStorage()

# Get data from fields
delete_data = form.getvalue('delete_data')
if not delete_data:  # set default value
    delete_data = 'yes'
push_data = form.getvalue('push_data')
if not push_data:  # set default value
    push_data = 'yes'
language1 = form.getvalue('language1')
language2 = form.getvalue('language2')
url1 = form.getvalue('url1')
url2 = form.getvalue('url2')
user_message = form.getvalue('user_message')  # TODO: filter the user_message to avoid email hacking
if user_message:
    user_message_file = os.path.join(base_data_dir, relative_user_dir, TEMP_RESULT_DIR,USER_NOTE_FILE)
    with codecs.open(user_message_file, encoding='utf-8', mode='w+') as new_file:
        new_file.write(('Lời nhắn:\n' + user_message).decode('utf-8'))

def save_uploaded_file(cgi_form, form_field, upload_dir, whitelist_ext):
    """This saves a file uploaded by an HTML form.
       The form_field is the name of the file input field from the form.
       For example, the following form_field would be "file_1":
           <input name="file_1" type="file">
       The upload_dir is the directory where the file will be written.
       The whitelist_ext is the set of allowed file extensions for uploading.
       If no file was uploaded or if the field does not exist then
       this does nothing.
    """
    if not cgi_form.has_key(form_field): return False
    file_item = cgi_form[form_field]
    if not file_item.file: return False
    # Strip leading path from file name to avoid
    # directory traversal attacks.
    # Replace \ by / to make sure compatibility with Windows path
    filename_base = os.path.basename(file_item.filename.replace("\\", "/"))
    mainname, extname = os.path.splitext(filename_base)  # mainname is '123.php.', extname is '.jpg'
    # Use white list of file type to be uploaded
    if not extname in whitelist_ext: return False
    # Replace non alpha numeric characters to _ for URL friendly
    mainname = re.sub('[^0-9a-zA-Z/]+', '_', mainname)
    # Replace . by _ to protect against double extension attacks which can activate PHP scripts
    filename_base = mainname.replace('.', '_') + extname
    file_path = os.path.join(upload_dir, filename_base)
    with open(file_path, 'wb') as outfile:
        shutil.copyfileobj(file_item.file, outfile)
        # outfile.write(file_item.file.read())
        # while 1:
        #     chunk = file_item.file.read(100000)
        #     if not chunk: break
        #     outfile.write (chunk)
    return filename_base

def send_html_email(config_file, list_html_files, subject='Email', additional_message=''):
    import yaml
    with open(config_file, 'r') as stream:
        config = yaml.load(stream)
    post_email_directory = config['email']['post_email_directory']
    server_email_address = config['email']['server_email']
    admin_email_address = config['email']['admin_email']
    secret_email_adress = config['email']['secret_email']
    tags = config['email']['tags']
    path.append(TOP_DIR + '/' + post_email_directory)
    exec('import ' + server_email_address)  # email address displayed in the To field
    exec('import ' + admin_email_address)  # email address(es) of the web admin(s), send together
    exec('import ' + secret_email_adress)  # email address(es) for wordpress posting is encoded here, send separately
    
    server_email = eval(server_email_address).server_email
    admin_email = eval(admin_email_address).admin_email
    secret_email = eval(secret_email_adress).secret_email
    
    # Send email to admin(s), main content is put in the attachment to send big emails (smtp of Hawkhost has some limitations with big email body text)
    message = additional_message
    if isinstance(admin_email, list):
        email_address = ', '.join(admin_email)
    else:
        email_address = admin_email
    Email_Tools.send_email_attachment(server_email, email_address, '', '', subject, tags + '\n' + additional_message, list_html_files)
        
    # Send content to email address for Wordpress auto posting, there are some notes:
    # - Email address must be in the To or Cc field, Wordpress does not process email if the address is in Bcc field. Hence send email(s) separately.
    # - If HTML content with Unicode is sent as HTML body of the email, Wordpress may not display on the blog correctly (some issue happened with Vietnamese)
    # - HTML content with Unicode should be sent as attachments, then Wordpress will parse and post the content of attachments
    if isinstance(secret_email, list):
        for secret_email_address in secret_email:
            Email_Tools.send_email_attachment(server_email, secret_email_address, '', '', subject, tags + '\n' + additional_message, list_html_files)
    else:
        Email_Tools.send_email_attachment(server_email, secret_email, '', '', subject, tags + '\n' + additional_message, list_html_files)
        
pdf_ext  = set(['.pdf', '.PDF'])
text_ext = set(['.docx', '.DOCX', '.txt', '.TXT', '.doc', '.DOC'])

white_list = set()
white_list = white_list.union(pdf_ext)
white_list = white_list.union(text_ext)

user_upload_dir = os.path.join(TOP_DIR, DATA_DIR, relative_user_dir, UPLOAD_DIR)
file_input_1 = save_uploaded_file(form, "upload1", user_upload_dir, white_list)
file_input_2 = save_uploaded_file(form, "upload2", user_upload_dir, white_list)

# Retrieve from URL if a file download was not successful
if not file_input_1 and url1 != '':
    file_1_basename = Url_Text.url_to_file_name(url1)
    txt_file_1 = os.path.join(user_upload_dir, file_1_basename)
    if Url_Text.get_text_url(url1, txt_file_1): # return None when retrieving not successful, hence "if" fails
        file_input_1 = file_1_basename

if not file_input_2 and url2 != '':
    file_2_basename = Url_Text.url_to_file_name(url2)
    txt_file_2 = os.path.join(user_upload_dir, file_2_basename)
    if Url_Text.get_text_url(url2, txt_file_2): # return None when retrieving not successful, hence "if" fails
        file_input_2 = file_2_basename

if file_input_1:
    file_1_path = os.path.join(ABS_TOP_DIR, DATA_DIR, relative_user_dir, UPLOAD_DIR, file_input_1)
    url_file_1 = BASE_URL+'/'+DATA_DIR+'/'+relative_user_dir+'/'+UPLOAD_DIR+'/'+file_input_1
    message_file_1 = 'File 1: '+file_input_1+' was uploaded/retrieved successfully.'
else:
    message_file_1 = 'File 1 is not an accepted file. It was not uploaded.'

if file_input_2:
    file_2_path = os.path.join(ABS_TOP_DIR, DATA_DIR, relative_user_dir, UPLOAD_DIR, file_input_2)
    url_file_2 = BASE_URL+'/'+DATA_DIR+'/'+relative_user_dir+'/'+UPLOAD_DIR+'/'+file_input_2
    message_file_2 = 'File 2: '+file_input_2+' was uploaded/retrieved successfully.'
else:
    message_file_2 = 'File 2 is not an accepted file. It was not uploaded.'

proceed_flag = file_input_1 and file_input_2 and file_input_1!=file_input_2 and os.path.splitext(file_input_1)[1]==os.path.splitext(file_input_2)[1]

if proceed_flag:
    # Convert encoding of TXT files to utf-8 to avoid error: <class '_csv.Error'>: line contains NULL byte (happening with UTF-16LE)
    txt_ext = set(['.txt', '.TXT'])
    if os.path.splitext(file_1_path)[1] in txt_ext:
        Text_Tools.convert_to_utf8(file_1_path)
    if os.path.splitext(file_2_path)[1] in txt_ext:
        Text_Tools.convert_to_utf8(file_2_path)
    
    # Processing text files and PDF files
    Auto_Align_Compare.auto_align_list_webcgi([file_1_path], [file_2_path], TOP_DIR+'/'+ALIGNER_DIR+'/LF_aligner_3.11.sh', language1, language2)

    # os.system('find ../data/uploads -maxdepth 2 -name aligned* > list_aligned_to_be_converted.txt')
    os.system('find '+TOP_DIR+'/'+DATA_DIR+'/'+relative_user_dir+'/'+UPLOAD_DIR+' -maxdepth 2 -name aligned* > '+TOP_DIR+'/'+DATA_DIR+'/'+relative_user_dir+'/'+UPLOAD_DIR+'/list_aligned_to_be_converted.txt')

    Auto_Align_Compare.auto_tab_txt_to_csv_list(TOP_DIR+'/'+DATA_DIR+'/'+relative_user_dir+'/'+UPLOAD_DIR+'/list_aligned_to_be_converted.txt')

    # os.system('find ../data/uploads -maxdepth 2 -name aligned* -exec mv {} ../data/results/ \;')
    os.system('find '+TOP_DIR+'/'+DATA_DIR+'/'+relative_user_dir+'/'+UPLOAD_DIR+' -maxdepth 2 -name aligned* -exec mv {} '+TOP_DIR+'/'+DATA_DIR+'/'+relative_user_dir+'/'+TEMP_RESULT_DIR+'/ \;')
    
    # Convert CSV to HTML
    list_html_files = []
    
    # First add user message if it exists
    if user_message: list_html_files.append(user_message_file)

    # csv_paths = glob.glob('../data/results/*.csv')
    csv_paths = glob.glob(TOP_DIR+'/'+DATA_DIR+'/'+relative_user_dir+'/'+TEMP_RESULT_DIR+'/*.csv')
    for csv_file in csv_paths:
        list_html_files.append(Csv_Html.csv_to_html(csv_file, csv_file+'.html'))
    
    # Send HTML results to data website, use relative_user_dir as the email subject
    if not push_data != 'yes':
        language_message = '[tags %s, %s]\n' % (LANGUAGE_DICT[language1], LANGUAGE_DICT[language2])
        send_html_email(TOP_DIR+'/config.yaml', list_html_files, relative_user_dir, language_message)

    ## This command doesn't delete CSV files with space in file name
    # os.system('rm '+path_to_delete) for path_to_delete in csv_paths]

    # Result file to be given back
    # os.system('rm ../data/results.zip')
    # os.system('rm '+TOP_DIR+'/'+DATA_DIR+'/'+relative_user_dir+'/'+OUTPUT_DIR+'/'+OUTPUT_FILE)
    # os.system('zip -r ../data/results.zip ../data/results')
    os.system('zip -r -j '+TOP_DIR+'/'+DATA_DIR+'/'+relative_user_dir+'/'+OUTPUT_DIR+'/'+OUTPUT_FILE+' '+TOP_DIR+'/'+DATA_DIR+'/'+relative_user_dir+'/'+TEMP_RESULT_DIR)

    url_file_result = BASE_URL+'/'+DATA_DIR+'/'+relative_user_dir+'/'+OUTPUT_DIR+'/'+OUTPUT_FILE


# Clean up
if not delete_data != 'yes':
    # os.system('rm -r ../data/uploads/*')
    os.system('rm -r '+TOP_DIR+'/'+DATA_DIR+'/'+relative_user_dir+'/'+UPLOAD_DIR+'/*')

# Output to the web
print "Content-type:text/html"
print ""
print "<html>"
print "<head>"
print "<title>Ket qua so sanh 2 file van ban de tao ra bang song ngu</title>"
print "</head>"
print "<body>"
print "<h1>Results of aligner</h1>"
print "<h2>%s</h2>" % (message_file_1)
if delete_data != 'yes' and file_input_1:
    print "<h2>Link to the uploaded file 1: <a href=\"%s\">%s</a></h2>" % (url_file_1, url_file_1)
print "<h2>%s</h2>" % (message_file_2)
if delete_data != 'yes' and file_input_2:
    print "<h2>Link to the uploaded file 2: <a href=\"%s\">%s</a></h2>" % (url_file_2, url_file_2)
if proceed_flag:
    print "<h2>Link to the ZIP file: <a href=\"%s\">%s</a></h2>" % (url_file_result, url_file_result)
    for html_file in list_html_files:
        url_html_file = BASE_URL+'/'+html_file
        print "<h2>HTML output file: <a href=\"%s\">%s</a></h2>" % (url_html_file, url_html_file)
    
if not push_data != 'yes':
    print "<h2>Bang song ngu da duoc dua den <a href=\"https://rosettavn.wordpress.com\">rosettavn.wordpress.com</a>.</h2>"
if not delete_data != 'yes':
    print "<h2>Data (except the result files) was deleted on the server.</h2>"
print "</body>"
print "</html>"
