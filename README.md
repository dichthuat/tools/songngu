# songngu

Chương trình này giúp so sánh 2 văn bản, ví dụ: tài liệu gốc và bản dịch.

Chương trình sẽ xuất ra bảng song ngữ để đối chiếu nội dung theo từng cặp câu, dưới dạng file HTML (có thể chép lại và đưa vào blog, trang web, file Word) và file ZIP chứa bảng ở định dạng CSV (mở được bằng Excel). Nó cũng có chức năng gửi kết quả qua email, giúp tự động chuyển dữ liệu vào trang blog như Wordpress.

This tool compares two documents to create a bilingual corpus, output as HTML, CSV, possibly integrated to a Wordpress blog via emails.

It is written to be executed from a Linux web host which runs Python (currently it supports Python 2.6) and Perl.

# Installation:

1. Copy the whole code base to your host. For example, if your Linux box has git:
```
[~/public_html]# git clone https://gitlab.com/dichthuat/tools/songngu.git
```
2. Put a symbolic link in your cgi-bin folder (mkdir this directory if not exist), to link to the processing script:
```
[~/public_html]# cd cgi-bin
[~/public_html/cgi-bin]# ln -s ../songngu/cgi-bin/songngu_processing.py songngu_processing.py
```
3. Put configuration of email sending to the right hidden place (not accessible by web URL):
```
[~/public_html/cgi-bin]# cd ../songngu
[~/public_html/songngu]# mv songngu_config_addon ../../
```
4. Change configuration data to your own:
```
[~/public_html/songngu]# cd ../../songngu_config_addon
[~/songngu_config_addon]# echo "secret_email = 'your_own_secret@post.wordpress.com'" > secret_email_lib.py
[~/songngu_config_addon]# echo "server_email = 'an_email@example.com'" > server_email_lib.py
[~/songngu_config_addon]# echo "admin_email = 'admin_email@example.com'" > admin_email_lib.py
[~/songngu_config_addon]# cd ../public_html/songngu
[~/public_html/songngu]# nano config.yml
```
and then change the configuration in config.yml, note the meaning:

```yaml
email:
    data_website: the web address that new results could be sent to (typically a Wordpress blog)
    post_email_directory: ../../songngu_config_addon
    server_email: (a Python library) containing email address which is displayed in "To:" field of emails to be sent from web host, in this example is server_email_lib.py
    admin_email: (a Python library) containing email address of whom the notifications of new results could be sent to, in this example is admin_email_lib.py
    secret_email: (a Python library) containing secret email address provided by Wordpress, in this example is secret_email_lib.py
    tags: additional tags included with emails to be sent to Wordpress blog, e.g. '[category new]'
```

* Note that in order to connect your songngu website with your Wordpress blog, you need to enable "Post by email" feature of your Wordpress blog and get your own secret email address (to put in the configuration at step 4). Read more about [post by email in Wordpress](https://en.support.wordpress.com/post-by-email/).

# How to use:

After installation, if the permission of script files are correct, you can access your website, e.g. http://example.com/songngu/

The interface on the songngu website should be simple to understand: choose 2 documents, e.g. a source text and a translated text, specify their languages, optionals such as whether the source data would be deleted immediately or the results would be forwarded to the data_website (Wordpress blog). Then click "Submit info".

If the user choose to forward results to the data_website, the results will be sent via email: a copy is sent to the web admin, another copy is sent to the secret email address (which would be posted as a new blog post by Wordpress).

# The workflow of songngu:

a/. When the songngu website receives two documents (either by uploading or providing URL so that it can download), it creates a temporary data folder and put source documents there.

b/. Then it calls `LF Aligner` with the correct parameters, to do the "align" action.

c/. LF Aligner is a Perl wrapper to the aligner-engine `hunalign`, together with some supporting tools (e.g. supplying bilingual dictionary to hunalign). It then passes the data to hunalign.

d/. hunalign compares sentences between the two source files, and outputs the bilingual result (called a "corpus") as a text file.

e/. LF Aligner receives that text file and converts it to a CSV file.

f/. songngu script then converts that CSV file into HTML file, to provide output for checking on web browser, such HTML file is also sent via email as attachment to the web admin and secret email (if the user chooses that option).

# Some tricky notes (for developers):

Steps a, b, f are executed with Python CGI (Python script needs to be executed in the cgi-bin directory) and its interaction with Linux shell. This code base also includes dependency Python modules for Python 2.6 running on CentOS 6 (tested on Hawkhost), like: lxml, xlrd, xlwt, chardet.

Steps c, e needs Perl (should be popularly supported on web hosts).

In step d, hunalign requires a shared library 32-bit ld-linux.so.2 that is typically not available on shared Linux 64-bit web hosts. In order to make this code run on shared web hosts, we include a precompiled ld-linux.so.2 (for CentOS 6 32-bit), and tweak the binary of hunalign to point to a relative ld-linux.so.2 instead of an absolute path one. This setting is also necessary for the script pdftotext (to extract text from PDF files), which was precompiled on Linux 32-bit.

In the Python processing script, there is also a command to change the LD_LIBRARY_PATH environment to the folder aligner/scripts, so that hunalign will load libstdc++.so.6 shipped with it, which was compiled for 32-bit architecture.

# TODO:

1. Make the code compatible to both Python 2 and Python 3.

2. Create an automated install.py script.

3. Refactor the code, so that it is straightforward to test in Linux console.

4. Periodically sanitize the data folder.
