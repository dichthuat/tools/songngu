#!/usr/bin/env python

import smtplib

def send_html_file_sendmail(from_addr, to_addr, subject, tags, html_file):
    import os
    os.system('(echo "To: ' + to_addr +'"; echo "Subject: ' + subject + '"; echo "Content-Type: text/html\nMIME-Version: 1.0"; echo ""; echo "' + tags + '"; cat ' + html_file +';) | sendmail -t')

def send_email_html(from_addr, to_addr_list, cc_addr_list, bcc_addr_list, subject, message, login='', password='', smtpserver='localhost'):
    """Send via Gmail:
    send_email_html(from_addr, to_addr_list, cc_addr_list, bcc_addr_list, subject, message, 'user_name@gmail.com', 'gmail_password', 'smtp.gmail.com:587')
    Simple send via localhost smtp:
    send_email_html(from_addr, to_addr_list, cc_addr_list, bcc_addr_list, subject, message)
    with to_addr_list, cc_addr_list, and bcc_addr_list are strings containing email addresses separated by comma (,)
    Example:
    send_email_html('sender@email.com', 'to1@email.com, to2@email.com', 'cc1@email.com, cc2@email.com', 'bcc1@email.com, bcc2@email.com', 'Test email', 'This is <b>HTML</b> email')
    """
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText

# me == my email address
# you == recipient's email address
    me = from_addr
    you = to_addr_list
    cc = cc_addr_list
    bcc = bcc_addr_list
    
# Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = subject
    msg['From'] = me
    msg['To'] = you
    msg['Cc'] = cc
    msg['Bcc'] = bcc
    
# Create the body of the message (a plain-text and an HTML version).
    #text = 'Test email'
    html = message

# Record the MIME types of both parts - text/plain and text/html.
    #part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

# Attach parts into message container.
# According to RFC 2046, the last part of a multipart message, in this case
# the HTML message, is best and preferred.
    #msg.attach(part1)
    msg.attach(part2)

# Send the message via SMTP server.
    s = smtplib.SMTP(smtpserver)
    if smtpserver!='localhost':
        s.starttls()
        s.login(login, password)

# sendmail function takes 3 arguments: sender's address, recipient's address
# and message to send - here it is sent as one string.
    s.sendmail(me, [you, cc, bcc], msg.as_string())
    s.quit()

def send_email_attachment(from_addr, to_addr_list, cc_addr_list, bcc_addr_list, subject, message, attach_list='', login='', password='', smtpserver='localhost'):
    from email.mime.multipart import MIMEMultipart
    from email.mime.text import MIMEText
    from email.mime.base import MIMEBase
    from email import encoders

# me == my email address
# you == recipient's email address
    me = from_addr
    you = to_addr_list
    cc = cc_addr_list
    bcc = bcc_addr_list

# Default encoding mode set to Quoted Printable. Acts globally!
    #Charset.add_charset('utf-8', Charset.QP, Charset.QP, 'utf-8')
    
# Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('mixed')
    #msg = MIMEMultipart('alternative')  # using 'alternative' would duplicate the attachment
    msg['Subject'] = subject
    msg['From'] = me
    msg['To'] = you
    msg['Cc'] = cc
    msg['Bcc'] = bcc
    
# Create the body of the message (a plain-text and an HTML version).
    #text = 'Test email'
    html = message

# Record the MIME types of both parts - text/plain and text/html. Then attach parts into message container.
    #part1 = MIMEText(text.encode('utf-8'), 'plain', 'UTF-8')  # to use encode with charset, to enforce utf-8 charset
    #msg.attach(part1)
    part2 = MIMEText(html, 'html')
    msg.attach(part2)

# Include the attachments
    if isinstance(attach_list, list):
        for attach_file in attach_list:
            attachment = open(attach_file,'rb')
            part3 = MIMEBase('application', 'octet-stream')
            part3.set_payload((attachment).read())
            encoders.encode_base64(part3)
            part3.add_header('Content-Type', 'text/html; charset="UTF-8"; name="' + attach_file +'"')
            part3.add_header('Content-Disposition', 'attachment; filename="' + attach_file +'"')  # mimic Gmail header, so that Wordpress would accept HTML attachment
            msg.attach(part3)
    else:
        attach_file = attach_list
        attachment = open(attach_file,'rb')
        part3 = MIMEBase('application', 'octet-stream')
        part3.set_payload((attachment).read())
        encoders.encode_base64(part3)
        part3.add_header('Content-Type', 'text/html; charset="UTF-8"; name="' + attach_file +'"')
        part3.add_header('Content-Disposition', 'attachment; filename="' + attach_file +'"')  # mimic Gmail header, so that Wordpress would accept HTML attachment
        msg.attach(part3)

# Send the message via SMTP server.
    s = smtplib.SMTP(smtpserver)
    if smtpserver!='localhost':
        s.starttls()
        s.login(login, password)

# sendmail function takes 3 arguments: sender's address, recipient's address
# and message to send - here it is sent as one string.
    s.sendmail(me, [you, cc, bcc], msg.as_string())
    s.quit()
