#!/usr/bin/env python
# -*- coding: utf-8 -*-


## Using BeautifulSoup, need to install python-bs4
#from bs4 import BeautifulSoup
#import urllib2
#url="http://www.viet-studies.net/kinhte/ChinaBadOldDays_FA_trans.html"
#page=urllib2.urlopen(url)
#html_page=page.read()
#soup=BeautifulSoup(html_page)
#textonly = soup.get_text() # textonly has Unix-style end-of-line

## Using lxml and requests
#import requests
#import codecs
#from lxml import html
#url="http://www.viet-studies.net/kinhte/ChinaBadOldDays_FA_trans.html"
#page = requests.get(url)
#tree = html.fromstring(page.content)
#newtext=''.join(tree.xpath('//text()')) # newtext DOS-style end-of-line
#with codecs.open('url_text_lxml.txt',encoding='utf-8', mode='w+') as file:
  #file.write(newtext)

## Download PDF file in URL with urllib
#import urllib
#import os
#pdf_url = 'http://www.viet-studies.net/kinhte/ChinaBadOldDays_FA.pdf'
#pdf_file = pdf_url[pdf_url.rfind("/")+1:] # 'ChinaBadOldDays_FA.pdf'
#urllib.urlretrieve(pdf_url, pdf_file)

# Using lxml and urllib2. urllib2 is available in CentOS 6 of Hawkhost
import urllib2
import lxml.html
import sys
import os
import re
#import codecs
import Text_Tools

def get_text_html(html_url, txt_file):
    """
    Return None if failed to access url, otherwise return text extracted and store the text in txt_file 
    """
    try:
        page=urllib2.urlopen(html_url)
        html_page=page.read()
        newtree=lxml.html.fromstring(html_page)
        text=''.join(newtree.xpath('//text()'))
        # Note: the EOL character is \r\n
        text2 = text.replace('\r\n', '\n')
        text3 = join_line(text2)
        with open(txt_file,'wt') as new_text_file:
            new_text_file.write(text3.encode('utf-8'))
    except urllib2.HTTPError as error:
        print('Caught this error: ' + repr(error))
        if error.code == 404:
            print('404, URL not found')
            return None
        else:
            raise
    return text3

def join_line(text):
    # Connect lines that could be splitted due to HTML/PDF extraction
    notab = text.replace('\t', '')
    replace_pattern = r'([^\n]) *\n(.)'
    clean_text = re.sub(replace_pattern,r'\g<1> \g<2>', notab)
    return clean_text

    #with codecs.open('clean_text.txt',encoding='utf-8', mode='w+') as new_text_file:
        #new_text_file.write(clean_text)


def download_file(download_url, file_save_path):
    response = urllib2.urlopen(download_url)
    with open(file_save_path, 'wb') as new_file:
        new_file.write(response.read())

def get_text_pdf(pdf_url, txt_file):
    """
    Return None if failed to download pdf file, otherwise return text extracted and store the text in txt_file 
    """
    try:
        mainname, extname = os.path.splitext(txt_file)
        pdf_file = mainname + '.pdf'
        download_file(pdf_url, pdf_file)
        # Make clean text from PDF file
        convert_command = 'pdftotext ' + pdf_file + ' ' + txt_file
        os.system(convert_command)
        # Depending on pdftotext version and OS, the txt_file could have different encoding
        # Tested 2018.11.07: on local Ubuntu 16.04 terminal, the file has encoding utf-8
        # while on host CentOS 6 terminal, the encoding is ISO-8859-2
        # Hence, we need to convert encoding to utf-8 for sure
        Text_Tools.convert_to_utf8(txt_file)
        with open(txt_file,'rt') as text_file:
            text = text_file.read().decode('utf-8')
        text2 = text.replace('\r\n', '\n')  # just for sure, pdftotext already saves file with EOL character \n
        text3 = join_line(text2)
        with open(txt_file,'wt') as new_text_file:
            new_text_file.write(text3.encode('utf-8'))
    except urllib2.HTTPError as error:
        print('Caught this error: ' + repr(error))
        if error.code == 404:
            print('404, URL not found')
            return None
        else:
            raise
    return text3

def get_text_url(url, txt_file):
    last_part_url = url[url.rfind("/")+1:]
    #ext_url = last_part_url[last_part_url.rfind(".")+1:]
    ## problem with: 'https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:1999:271:0028:0040:EN:PDF'
    ## last_part_url: 'LexUriServ.do?uri=OJ:L:1999:271:0028:0040:EN:PDF'
    ## ext_url: 'do?uri=OJ:L:1999:271:0028:0040:EN:PDF'
    ext_url = last_part_url[-3:]
    if ext_url.lower() == 'pdf':
        text = get_text_pdf(url, txt_file)
    else:       
        text = get_text_html(url, txt_file)
    return text

def url_to_file_name(url):
    """
    Return filename.txt with filename extracted from url
    """
    last_part_url = url[url.rfind("/")+1:]  # if "/" not found, then return the entire url
    mainname, extname = os.path.splitext(last_part_url)
    if mainname == '':  # for the case without sub-path, like: www.example.com/
        mainname, extname = os.path.splitext(url)
    # Replace non alpha numeric characters to _ for URL friendly
    #mainname = re.sub('[^0-9a-zA-Z]+', '_', mainname)
    ## problem with:
    ## url1 = 'https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:1999:271:0028:0040:EN:PDF'
    ## url2 = https://eur-lex.europa.eu/LexUriServ/LexUriServ.do?uri=OJ:L:1999:271:0028:0040:DE:PDF
    ## last_part_url2: 'LexUriServ.do?uri=OJ:L:1999:271:0028:0040:EN:PDF'
    ## last_part_url1: 'LexUriServ.do?uri=OJ:L:1999:271:0028:0040:DE:PDF'
    ## both give the SAME mainname: LexUriServ
    ## then if we compare these two URL, the same mainname lead to the same file -> cannot be compared
    # Use url instead of mainname
    filtered_name = re.sub('[^0-9a-zA-Z]+', '_', url)
    txt_file = filtered_name + '.txt'
    return txt_file

# Main operation, when calling: python Url_Text.py url text_file
if __name__ == "__main__":
    url = str(sys.argv[1])
    if len(sys.argv)>2:
        text_file = str(sys.argv[2])
    else:
        text_file = url_to_file_name(url)
    extracted_text = get_text_url(url, text_file)
    print('Text from ' + url + ' was retrieved to the file ' + text_file)
    print(extracted_text)
