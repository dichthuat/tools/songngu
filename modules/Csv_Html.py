#!/usr/bin/env python
# -*- coding: utf-8 -*-

# csv2html.py
# CSV to HTML Converter

import csv
import sys

table_indent_num = 2
tr_indent_num = 4
td_indent_num = 6
white_space = ' '
meta = '<head>\n\
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>\n\
</head>'

def csv_to_html(csvFile, htmlFile='output.html'):
    """Convert Csv file to HTML file with simple table
    Example:
    csv_to_html('v27_002-v30_002.csv', 'v27_002-v30_002.html')
    """

    csv_reader = csv.reader(open(csvFile), delimiter='\t', quoting=csv.QUOTE_ALL)
    table_indent = white_space * table_indent_num
    tr_indent = white_space * tr_indent_num
    td_indent = white_space * td_indent_num

    with open(htmlFile, 'wt') as newfile:
        newfile.write(meta + '\n')
        newfile.write(table_indent + '<table border="1">' + '\n')
        for i, row in enumerate(csv_reader):
            newfile.write(tr_indent + '<tr>' + '\n')
            
            # Uncomment the following two lines if you don't
            # want a column for indexes
            if i == 0: newfile.write(td_indent + '<th>#</th>' + '\n')
            else: newfile.write(td_indent + '<td>' + str(i) + '</td>' + '\n')
            
            # Assume that the first line is a header
            for column in row:
                # Note: if empty cell or only with a space, then Wordpress will not treat it as a cell,
                #       hence Wordpress will shift the cell from the right to this position.
                #       To circumvent this issue of Wordpress: put a special white character if the cell is empty.
                if column=='': column=' '  # figure space U+2007 in https://www.brunildo.org/test/space-chars.html
                if i == 0: newfile.write(td_indent + '<th>' + column + '</th>' + '\n')
                else: newfile.write(td_indent + '<td>' + column + '</td>' + '\n')
            
            newfile.write(tr_indent + '</tr>' + '\n')
        
        newfile.write(table_indent + '</table>' + '\n')
        
    return htmlFile

if __name__ == '__main__':
    argn = len(sys.argv)
    if argn != 2:
        print('Usage: python csv2html.py <CSV file>')
        exit(1)

    csv_to_html(sys.argv[1])
